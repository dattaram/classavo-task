import React, { useState, useMemo } from 'react'
import imageExtensions from 'image-extensions'
import isUrl from 'is-url'
import { Transforms, createEditor } from 'slate'
import {
  Slate,
  Editable,
  useEditor,
  useSelected,
  useFocused,
  withReact,
} from 'slate-react'
import { withHistory } from 'slate-history'

export default function TestTwo() {
    const [value,setValue]=useState('hello')
    const editor = useMemo(
      () => withImages(withHistory(withReact(createEditor()))),
      []
    )
    return (
        <div>
            <Slate editor={editor} value={value} onChange={value => setValue(value)}>
                <Editable
                    // renderElement={props => <Element {...props} />}
                    placeholder="Enter some text..."
                />
            </Slate>
        </div>
    )
}

const withImages = editor => {
    const { insertData, isVoid } = editor
  
    editor.isVoid = element => {
      return element.type === 'image' ? true : isVoid(element)
    }
  
    editor.insertData = data => {
      const text = data.getData('text/plain')
      const { files } = data
  
      if (files && files.length > 0) {
        for (const file of files) {
          const reader = new FileReader()
          const [mime] = file.type.split('/')
  
          if (mime === 'image') {
            reader.addEventListener('load', () => {
              const url = reader.result
              insertImage(editor, url)
            })
  
            reader.readAsDataURL(file)
          }
        }
      } else if (isImageUrl(text)) {
        insertImage(editor, text)
      } else {
        insertData(data)
      }
    }
  
    return editor
  }
